package pl.danielfrak.chowplan;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.danielfrak.chowplan.application.exceptions.UserUnauthorizedException;
import pl.danielfrak.chowplan.domain.grocerylist.grocerylist.GroceryListGenerator;
import pl.danielfrak.chowplan.domain.grocerylist.items.Item;
import pl.danielfrak.chowplan.domain.grocerylist.meals.ChosenMeal;
import pl.danielfrak.chowplan.domain.grocerylist.meals.Ingredient;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ChowPlanApplicationTests {

    private final GroceryListGenerator generator = new GroceryListGenerator();

    @Test
    public void generateFromMeals() throws UserUnauthorizedException {
        List<Ingredient> expectedIngredients = createExpectedIngredients();

        List<ChosenMeal> meals = createMeals();
        assertEquals(generator.fromMeals(meals), expectedIngredients);
    }

    private List<ChosenMeal> createMeals() {
        ChosenMeal meal1 = createFirstMeal();
        ChosenMeal meal2 = createSecondMeal();

        return Arrays.asList(meal1, meal2);
    }

    private ChosenMeal createSecondMeal() {
        ChosenMeal meal2 = new ChosenMeal();
        meal2.setQuantity(1);

        meal2.setIngredients(Arrays.asList(
                createIngredient(1L, "Item 1", "", 2),
                createIngredient(3L, "Item 3", "ml", 3)
        ));
        return meal2;
    }

    private ChosenMeal createFirstMeal() {
        ChosenMeal meal1 = new ChosenMeal();
        meal1.setQuantity(5);

        meal1.setIngredients(Arrays.asList(
                createIngredient(1L, "Item 1", "", 1),
                createIngredient(2L, "Item 2", "kg", 1)
        ));
        return meal1;
    }

    private List<Ingredient> createExpectedIngredients() {
        return Arrays.asList(
                createIngredient(1L, "Item 1", "", 7),
                createIngredient(2L, "Item 2", "kg", 5),
                createIngredient(3L, "Item 3", "ml", 3)

        );
    }

    private Ingredient createIngredient(long id, String name, String unit, int quantity) {
        Item item = new Item(id, name, unit, null);
        Ingredient ingredient = new Ingredient(item, quantity);
        ingredient.setId(id);
        return ingredient;
    }

}
