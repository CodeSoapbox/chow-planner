package pl.danielfrak.chowplan.domain.grocerylist.items;

import org.junit.Test;

import static org.junit.Assert.*;

public class ItemTest {

    @Test
    public void equals() {
        Item item1 = new Item(1L, "Item", "", null);
        Item item2 = new Item(1L, "Item", "", null);

        assertEquals(item1, item2);
    }

    @Test
    public void notEquals() {
        Item item1 = new Item(1L, "Item 1", "", null);
        Item item2 = new Item(1L, "Item 2", "", null);

        assertNotEquals(item1, item2);
    }
}