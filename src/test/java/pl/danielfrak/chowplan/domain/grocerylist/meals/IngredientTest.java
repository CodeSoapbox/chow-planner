package pl.danielfrak.chowplan.domain.grocerylist.meals;

import org.junit.Test;
import pl.danielfrak.chowplan.domain.grocerylist.items.Item;

import static org.junit.Assert.*;

public class IngredientTest {

    @Test
    public void equals() {
        Item item1 = new Item(1L, "Item", "", null);
        Ingredient ingredient1 = new Ingredient(item1, 1);
        Ingredient ingredient2 = new Ingredient(item1, 1);
        assertEquals(ingredient1, ingredient2);
    }

    @Test
    public void notEquals() {
        Item item1 = new Item(1L, "Item 1", "", null);
        Item item2 = new Item(1L, "Item 2", "", null);
        Ingredient ingredient1 = new Ingredient(item1, 1);
        Ingredient ingredient2 = new Ingredient(item2, 1);
        assertNotEquals(ingredient1, ingredient2);
    }
}