package pl.danielfrak.chowplan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import pl.danielfrak.chowplan.application.configuration.DbDataInitializer;

@SpringBootApplication
public class ChowPlanApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(ChowPlanApplication.class, args);

        DbDataInitializer dbDataInitializer = context.getBean(DbDataInitializer.class);
        dbDataInitializer.prepareDatabaseData();
    }
}
