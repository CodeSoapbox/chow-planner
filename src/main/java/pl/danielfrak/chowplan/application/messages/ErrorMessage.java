package pl.danielfrak.chowplan.application.messages;

public class ErrorMessage extends UserMessage {
    public ErrorMessage(String message) {
        super(message, MessageType.ERROR);
    }
}
