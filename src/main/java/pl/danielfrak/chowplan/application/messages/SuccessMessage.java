package pl.danielfrak.chowplan.application.messages;

public class SuccessMessage extends UserMessage {
    public SuccessMessage(String message) {
        super(message, MessageType.SUCCESS);
    }
}
