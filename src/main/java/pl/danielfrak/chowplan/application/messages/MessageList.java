package pl.danielfrak.chowplan.application.messages;

import java.util.ArrayList;
import java.util.List;

public class MessageList {
    private List<UserMessage> messages;

    public MessageList() {
        messages = new ArrayList<>();
    }

    public MessageList(UserMessage message) {
        messages = new ArrayList<>();
        messages.add(message);
    }

    public List<UserMessage> getMessages() {
        return messages;
    }

    public void addMessage(UserMessage message) {
        messages.add(message);
    }
}
