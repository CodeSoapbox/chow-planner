package pl.danielfrak.chowplan.application.messages;

public class UserMessage {
    private String message;
    private MessageType type;

    private UserMessage() {
    }

    public UserMessage(String message, MessageType type) {
        this.message = message;
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type.toString();
    }
}
