package pl.danielfrak.chowplan.application.messages;

public class InfoMessage extends UserMessage {
    public InfoMessage(String message) {
        super(message, MessageType.INFO);
    }
}
