package pl.danielfrak.chowplan.application.messages;

public enum MessageType {
    INFO("info"),
    SUCCESS("success"),
    ERROR("error");

    private String type;

    MessageType(String type) {
        this.type = type;
    }

    public String toString() {
        return type;
    }
}
