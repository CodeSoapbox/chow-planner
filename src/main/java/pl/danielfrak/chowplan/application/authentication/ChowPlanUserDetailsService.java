package pl.danielfrak.chowplan.application.authentication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.danielfrak.chowplan.domain.grocerylist.cooks.Cook;
import pl.danielfrak.chowplan.domain.grocerylist.cooks.CookRepository;

@Service
public class ChowPlanUserDetailsService implements UserDetailsService {

    private CookRepository cookRepository;

    @Autowired
    public ChowPlanUserDetailsService(CookRepository cookRepository) {
        this.cookRepository = cookRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Cook cook = cookRepository.findByUsernameWithKitchens(username);

        if (cook == null) {
            throw new UsernameNotFoundException(username);
        }

        return new UserPrincipal(cook);
    }
}
