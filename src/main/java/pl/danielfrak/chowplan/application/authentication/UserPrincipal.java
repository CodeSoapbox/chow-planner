package pl.danielfrak.chowplan.application.authentication;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import pl.danielfrak.chowplan.application.exceptions.UserUnauthorizedException;
import pl.danielfrak.chowplan.domain.grocerylist.cooks.Cook;
import pl.danielfrak.chowplan.domain.grocerylist.kitchens.Kitchen;

import java.util.Arrays;
import java.util.Collection;

public class UserPrincipal implements UserDetails {
    private Cook cook;

    public UserPrincipal(Cook cook) {
        this.cook = cook;
    }

    public String getActiveKitchenName() {
        return this.cook.getActiveKitchen().getName();
    }

    public void updateCook(Cook cook) {
        this.cook = cook;
    }

    public Long getId() {
        return cook.getId();
    }

    public Kitchen getActiveKitchen() {
        return cook.getActiveKitchen();
    }

    public Cook getCook() {
        return cook;
    }

    public boolean hasKitchen(Long kitchenId) throws UserUnauthorizedException {
        return cook.getKitchens()
                .stream()
                .anyMatch(kitchen -> kitchen.getId().equals(kitchenId));
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority("ROLE_USER");
        return Arrays.asList(authority);
    }

    @Override
    public String getPassword() {
        return cook.getPassword();
    }

    @Override
    public String getUsername() {
        return cook.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public boolean getHintsEnabled() {
        return cook.getShowHints();
    }
}
