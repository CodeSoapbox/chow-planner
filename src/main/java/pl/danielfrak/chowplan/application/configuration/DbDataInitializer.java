package pl.danielfrak.chowplan.application.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.danielfrak.chowplan.domain.grocerylist.cooks.Cook;
import pl.danielfrak.chowplan.domain.grocerylist.cooks.CookRepository;
import pl.danielfrak.chowplan.domain.grocerylist.items.Item;
import pl.danielfrak.chowplan.domain.grocerylist.items.ItemRepository;
import pl.danielfrak.chowplan.domain.grocerylist.kitchens.Kitchen;
import pl.danielfrak.chowplan.domain.grocerylist.kitchens.KitchenRepository;
import pl.danielfrak.chowplan.domain.grocerylist.meals.Ingredient;
import pl.danielfrak.chowplan.domain.grocerylist.meals.Meal;
import pl.danielfrak.chowplan.domain.grocerylist.meals.MealRepository;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Service
public class DbDataInitializer {
    private final CookRepository cookRepository;

    private final ItemRepository itemRepository;

    private final KitchenRepository kitchenRepository;

    private final MealRepository mealRepository;

    private final PasswordEncoder passwordEncoder;

    private Cook demoCook;
    private Cook otherUserCook;
    private Kitchen firstKitchen;
    private Kitchen secondKitchen;
    private Kitchen myKitchen;

    @Autowired
    public DbDataInitializer(
            CookRepository cookRepository,
            KitchenRepository kitchenRepository,
            ItemRepository itemRepository,
            MealRepository mealRepository,
            PasswordEncoder passwordEncoder
    ) {
        this.cookRepository = cookRepository;
        this.kitchenRepository = kitchenRepository;
        this.itemRepository = itemRepository;
        this.mealRepository = mealRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public void prepareDatabaseData() {
        prepareUserData();
        prepareKitchenData();
        prepareItemData();
        prepareMealData();
    }

    private void prepareKitchenData() {
        firstKitchen = new Kitchen(null, "First kitchen", demoCook);
        kitchenRepository.save(firstKitchen);

        secondKitchen = new Kitchen(null, "Second kitchen", demoCook);
        kitchenRepository.save(secondKitchen);

        myKitchen = new Kitchen(null, "My kitchen", otherUserCook);
        kitchenRepository.save(myKitchen);

        setActiveKitchens();
    }

    private void setActiveKitchens() {
        demoCook.setActiveKitchen(firstKitchen);
        cookRepository.save(demoCook);

        otherUserCook.setActiveKitchen(myKitchen);
        cookRepository.save(otherUserCook);
    }

    private void prepareUserData() {
        demoCook = new Cook(null, "demo", passwordEncoder.encode("demo"));
        cookRepository.save(demoCook);

        otherUserCook = new Cook(null, "otheruser", passwordEncoder.encode("otheruser"));
        cookRepository.save(otherUserCook);
    }

    private void prepareItemData() {
        Map<String, String> items = new HashMap<>();
        items.put("Water", "L");
        items.put("Rice", "L");
        items.put("Milk", "L");
        items.put("Flour", "kg");
        items.put("Ham", "");
        items.put("Chicken breast", "kg");
        items.put("Pork loin", "kg");
        items.put("Minced meat", "kg");
        items.put("Salt", "Teaspoon(s)");
        items.put("Pepper", "g");
        items.put("Herbes de Provence", "g");
        items.put("Oregano", "g");
        items.put("Basil", "g");
        items.put("Thyme", "g");
        items.put("Milk chocolate", "g");
        items.put("Dark chocolate", "g");
        items.put("Paprika", "g");
        items.put("Onion", "");
        items.put("Potatoes", "kg");
        items.put("Zucchini", "");
        items.put("Cucumber", "");
        items.put("Gouda Cheese", "");
        items.put("Grana Padano Cheese", "");
        items.put("Lettuce", "");
        items.put("Apple", "");
        items.put("French fries", "kg");
        items.put("Millet", "kg");
        items.put("Sausage", "kg");
        items.put("Ketchup", "g");
        items.put("Tomato", "");
        items.put("Banana", "");
        items.put("Natural yoghurt", "Spoon(s)");
        items.put("Mustard", "Teaspoon(s)");
        items.put("Honey", "Teaspoon(s)");
        items.put("Cooking oil", "Spoon(s)");
        items.put("Olive oil", "Spoon(s)");
        items.put("Butter", "Spoon(s)");
        items.put("Yeast", "g");
        items.put("Warm water", "glass");
        items.put("Sugar", "Teaspoon(s)");
        items.put("White wine", "L");
        items.put("Garlic", "Clove(s)");
        items.put("Sliced tomato", "g");
        items.put("Tomato paste", "g");
        items.put("Carrot", "");

        addKitchenItems(items, firstKitchen);
        addKitchenItems(items, myKitchen);
    }

    private void addKitchenItems(Map<String, String> items, Kitchen kitchen) {
        items.entrySet().forEach(itemData -> {
            Item item = new Item(null, itemData.getKey(), itemData.getValue(), kitchen);
            itemRepository.save(item);
        });
    }

    private void prepareMealData() {
        prepareRiceRecipe();
        prepareBakedPorkchopRecipe();
        preparePizzaRecipe();
        prepareSpaghettiBologneseRecipe();
    }

    private void prepareSpaghettiBologneseRecipe() {
        Meal spaghettiBolognese = new Meal(null, "Spaghetti bolognese", firstKitchen);
        spaghettiBolognese.setRecipe("https://www.bbcgoodfood.com/recipes/1502640/the-best-spaghetti-bolognese");
        spaghettiBolognese.setIngredients(Arrays.asList(
                new Ingredient(itemRepository.findByNameAndKitchen("White wine", firstKitchen), .5),
                new Ingredient(itemRepository.findByNameAndKitchen("Onion", firstKitchen), 1),
                new Ingredient(itemRepository.findByNameAndKitchen("Garlic", firstKitchen), 2),
                new Ingredient(itemRepository.findByNameAndKitchen("Minced meat", firstKitchen), .5),
                new Ingredient(itemRepository.findByNameAndKitchen("Olive oil", firstKitchen), 1),
                new Ingredient(itemRepository.findByNameAndKitchen("Salt", firstKitchen), .1),
                new Ingredient(itemRepository.findByNameAndKitchen("Pepper", firstKitchen), .1),
                new Ingredient(itemRepository.findByNameAndKitchen("Oregano", firstKitchen), 10),
                new Ingredient(itemRepository.findByNameAndKitchen("Thyme", firstKitchen), 10),
                new Ingredient(itemRepository.findByNameAndKitchen("Basil", firstKitchen), 10),
                new Ingredient(itemRepository.findByNameAndKitchen("Sliced tomato", firstKitchen), 800),
                new Ingredient(itemRepository.findByNameAndKitchen("Tomato paste", firstKitchen), 400),
                new Ingredient(itemRepository.findByNameAndKitchen("Carrot", firstKitchen), 2)
        ));
        mealRepository.save(spaghettiBolognese);
    }

    private void preparePizzaRecipe() {
        Meal pizza = new Meal(null, "Pizza", firstKitchen);
        pizza.setRecipe("Do mąki dodajemy oliwę, sól i mieszamy, aby w mące pojawiły się malutkie grudki.\n" +
                "W miseczce łączymy wodę, cukier i drożdże. Bardzo energicznie łączymy i odstawiamy na 2-3 min, aby drożdże zaczęły działać. Jeżeli robimy ze świeżymi drożdżami odstawiamy na 30-40 min.\n" +
                "Rozczyn dodajemy do mąki i wyrabiamy robotem kuchennym lub ręcznie na elastyczne ciasto przez ok. 7-10 min. Po tym ciasto przekładamy do posmarowanej oliwą misy i odstawiamy pod przykryciem na 30 min.\n" +
                "Po wyrośnięciu ciasto dzielimy na 4 równe części, formujemy kule, oprószamy mąką i odstawiamy pod przykryciem na 30 min. Następnie z każdej części ciasta możemy piec pizzę.\n" +
                "Wyrośnięte ciasto wałkujemy na placek o średnicy ok. 20 cm i grubości 3-5 mm. Boki robimy lekko grubsze. Placek przekładamy na blachę do pieczenia, lub formę do pizzy, lub kamień do pizzy. Po tym powierzchnię pizzy smarujemy sosem pomidorowym i na nim umieszczamy ulubione dodatki. Pizzę pieczemy w rozgrzanym do 250-270 stopni (funkcja Góra+Dół) piekarniku przez ok. 7-9 min, aż brzegi pizzy znacznie zbrązowieją. Przed dostaniem pizzy z piekarnika należy sprawdzić, czy dno jest ładnie zrumienione, jeżeli jest światłe, to pieczemy dłużej, aż się zrumieni.");
        pizza.setIngredients(Arrays.asList(
                new Ingredient(itemRepository.findByNameAndKitchen("Flour", firstKitchen), .5),
                new Ingredient(itemRepository.findByNameAndKitchen("Olive oil", firstKitchen), 2),
                new Ingredient(itemRepository.findByNameAndKitchen("Salt", firstKitchen), 1),
                new Ingredient(itemRepository.findByNameAndKitchen("Yeast", firstKitchen), 7),
                new Ingredient(itemRepository.findByNameAndKitchen("Warm water", firstKitchen), 1),
                new Ingredient(itemRepository.findByNameAndKitchen("Sugar", firstKitchen), 2)
        ));
        mealRepository.save(pizza);
    }

    private void prepareBakedPorkchopRecipe() {
        Meal bakedPorkChop = new Meal(null, "Baked pork chop", firstKitchen);
        bakedPorkChop.setRecipe("");
        bakedPorkChop.setIngredients(Arrays.asList(
                new Ingredient(itemRepository.findByNameAndKitchen("Potatoes", firstKitchen), .25),
                new Ingredient(itemRepository.findByNameAndKitchen("Pork loin", firstKitchen), .5),
                new Ingredient(itemRepository.findByNameAndKitchen("Cucumber", firstKitchen), .25),
                new Ingredient(itemRepository.findByNameAndKitchen("Natural yoghurt", firstKitchen), 4),
                new Ingredient(itemRepository.findByNameAndKitchen("Mustard", firstKitchen), 1),
                new Ingredient(itemRepository.findByNameAndKitchen("Honey", firstKitchen), .5),
                new Ingredient(itemRepository.findByNameAndKitchen("Herbes de Provence", firstKitchen), .1),
                new Ingredient(itemRepository.findByNameAndKitchen("Salt", firstKitchen), 2),
                new Ingredient(itemRepository.findByNameAndKitchen("Cooking oil", firstKitchen), 10),
                new Ingredient(itemRepository.findByNameAndKitchen("Butter", firstKitchen), 5)
        ));
        mealRepository.save(bakedPorkChop);
    }

    private void prepareRiceRecipe() {
        Meal rice = new Meal(null, "Rice", firstKitchen);
        rice.setRecipe("Pour the rice into a pot.\n"
                .concat("Pour hot water into the pot and add a pinch of salt.\n")
                .concat("Put on the lid and cook on low heat until tall bubbles start to form.\n")
                .concat("Once you see the bubbles, turn off the burner and let sit until all remaining water is absorbed by the rice."));
        rice.setIngredients(Arrays.asList(
                new Ingredient(itemRepository.findByNameAndKitchen("Water", firstKitchen), .25),
                new Ingredient(itemRepository.findByNameAndKitchen("Rice", firstKitchen), .125)
        ));

        mealRepository.save(rice);
    }
}
