package pl.danielfrak.chowplan.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import pl.danielfrak.chowplan.application.authentication.UserPrincipal;
import pl.danielfrak.chowplan.application.exceptions.NoSuchKitchenException;
import pl.danielfrak.chowplan.application.exceptions.UserUnauthorizedException;
import pl.danielfrak.chowplan.domain.grocerylist.cooks.Cook;
import pl.danielfrak.chowplan.domain.grocerylist.cooks.CookRepository;
import pl.danielfrak.chowplan.domain.grocerylist.kitchens.Kitchen;
import pl.danielfrak.chowplan.domain.grocerylist.kitchens.KitchenRepository;

import java.util.Arrays;
import java.util.HashSet;

@Service
public class KitchenService {
    private final KitchenRepository kitchenRepository;
    private final CookRepository cookRepository;
    private final UserPrincipalProvider userPrincipalProvider;

    @Autowired
    public KitchenService(
            KitchenRepository kitchenRepository,
            CookRepository cookRepository,
            UserPrincipalProvider userPrincipalProvider
    ) {
        this.kitchenRepository = kitchenRepository;
        this.cookRepository = cookRepository;
        this.userPrincipalProvider = userPrincipalProvider;
    }

    public Iterable<Kitchen> getKitchens() throws UserUnauthorizedException {
        UserPrincipal userPrincipal = userPrincipalProvider.get();
        return kitchenRepository.findByCooksId(userPrincipal.getId());
    }

    public Kitchen getActiveKitchen() throws UserUnauthorizedException {
        UserPrincipal userPrincipal = userPrincipalProvider.get();
        return userPrincipal.getActiveKitchen();
    }

    public void setActiveKitchen(Long id) throws UserUnauthorizedException {
        UserPrincipal userPrincipal = userPrincipalProvider.get();

        Cook cook = cookRepository.findOneWithKitchens(userPrincipal.getId());
        cook.setActiveKitchen(kitchenRepository.findOne(id));
        cookRepository.save(cook);

        userPrincipal.updateCook(cook);
    }

    public Kitchen getKitchen(Long id) throws UserUnauthorizedException {
        Kitchen kitchen = kitchenRepository.findOne(id);

        if (kitchen == null || userIsNotAuthorizedToAccess(kitchen)) {
            throw new UserUnauthorizedException();
        }

        return kitchen;
    }

    private boolean userIsNotAuthorizedToAccess(Kitchen kitchen) throws UserUnauthorizedException {
        try {
            Long userId = userPrincipalProvider.get().getId();

            return !kitchen
                    .getCooks()
                    .stream()
                    .anyMatch(cook -> cook.getId().equals(userId));
        } catch (UserUnauthorizedException e) {
            return true;
        }
    }

    public void updateKitchen(Kitchen kitchen) throws NoSuchKitchenException, UserUnauthorizedException {
        if (kitchen == null || userIsNotAuthorizedToAccess(kitchen)) {
            throw new UserUnauthorizedException();
        }

        if (kitchen.getId() == null) {
            throw new NoSuchKitchenException();
        }

        kitchenRepository.save(kitchen);

        userPrincipalProvider.refresh();
    }

    public void addKitchen(Kitchen kitchen) throws UserUnauthorizedException {
        try {
            kitchen.setCooks(new HashSet<>(Arrays.asList(userPrincipalProvider.get().getCook())));
            kitchenRepository.save(kitchen);
        } catch (DataAccessException e) {
            throw new RuntimeException("msg.internal.error", e);
        }
    }

    public void deleteKitchen(Long id) throws UserUnauthorizedException {
        try {
            Kitchen kitchen = kitchenRepository.findOneWithCooks(id);

            if (kitchen.getCooks().size() > 1 || !userOwnsKitchen(kitchen)) {
                throw new UserUnauthorizedException();
            }

            kitchenRepository.delete(id);
        } catch (DataAccessException e) {
            throw new RuntimeException("msg.internal.error", e);
        }
    }

    private boolean userOwnsKitchen(Kitchen kitchen) throws UserUnauthorizedException {
        Long id = userPrincipalProvider.get().getId();

        return kitchen
                .getCooks()
                .stream()
                .anyMatch(cook -> cook.getId().equals(id));
    }
}
