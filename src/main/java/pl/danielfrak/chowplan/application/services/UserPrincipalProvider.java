package pl.danielfrak.chowplan.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import pl.danielfrak.chowplan.application.authentication.UserPrincipal;
import pl.danielfrak.chowplan.application.exceptions.UserUnauthorizedException;
import pl.danielfrak.chowplan.domain.grocerylist.cooks.CookRepository;

@Service
public class UserPrincipalProvider {

    private final CookRepository cookRepository;

    @Autowired
    public UserPrincipalProvider(CookRepository cookRepository) {
        this.cookRepository = cookRepository;
    }

    public UserPrincipal get() throws UserUnauthorizedException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserPrincipal userPrincipal = (UserPrincipal) auth.getPrincipal();

        if(userPrincipal == null) {
            throw new UserUnauthorizedException();
        }

        return userPrincipal;
    }

    public UserPrincipal getFresh() throws UserUnauthorizedException {
        UserPrincipal userPrincipal = get();
        userPrincipal.updateCook(cookRepository.findOneWithKitchens(userPrincipal.getId()));
        return userPrincipal;
    }

    public void refresh() throws UserUnauthorizedException {
        getFresh();
    }
}
