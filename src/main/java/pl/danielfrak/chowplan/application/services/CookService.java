package pl.danielfrak.chowplan.application.services;

import org.springframework.stereotype.Service;
import pl.danielfrak.chowplan.application.authentication.UserPrincipal;
import pl.danielfrak.chowplan.application.exceptions.UserUnauthorizedException;
import pl.danielfrak.chowplan.domain.grocerylist.cooks.Cook;
import pl.danielfrak.chowplan.domain.grocerylist.cooks.CookRepository;
import pl.danielfrak.chowplan.domain.grocerylist.kitchens.Kitchen;

@Service
public class CookService extends UserAwareEntityService {
    private final CookRepository cookRepository;

    public CookService(UserPrincipalProvider userPrincipalProvider, CookRepository cookRepository) {
        super(userPrincipalProvider);
        this.cookRepository = cookRepository;
    }

    public boolean getHintsEnabled() throws UserUnauthorizedException {
        UserPrincipal userPrincipal = userPrincipalProvider.get();
        Cook cook = userPrincipal.getCook();
        return cook.getShowHints();
    }

    public void setHintsEnabled(boolean hintsEnabled) throws UserUnauthorizedException {
        UserPrincipal userPrincipal = userPrincipalProvider.getFresh();

        Cook cook = userPrincipal.getCook();
        cook.setShowHints(hintsEnabled);
        cookRepository.save(cook);
    }

    protected void checkIfUserOwnsHisActiveKitchen() throws UserUnauthorizedException {
        UserPrincipal userPrincipal = userPrincipalProvider.getFresh();
        Kitchen activeKitchen = userPrincipalProvider.get().getActiveKitchen();
        if(activeKitchen == null) {
            throw new RuntimeException("msg.internal.error");
        }
        Long activeKitchenId = activeKitchen.getId();
        if(!userPrincipal.hasKitchen(activeKitchenId)) {
            throw new UserUnauthorizedException();
        }
    }
}
