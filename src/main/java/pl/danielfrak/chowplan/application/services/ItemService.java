package pl.danielfrak.chowplan.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.danielfrak.chowplan.application.exceptions.UserUnauthorizedException;
import pl.danielfrak.chowplan.domain.grocerylist.items.Item;
import pl.danielfrak.chowplan.domain.grocerylist.items.ItemRepository;

@Service
public class ItemService extends UserAwareEntityService {
    private ItemRepository repository;

    @Autowired
    public ItemService(
            final ItemRepository repository,
            final UserPrincipalProvider userPrincipalProvider
    ) {
        super(userPrincipalProvider);
        this.repository = repository;
    }

    public void addItem(final Item item) throws UserUnauthorizedException {
        try {
            item.setKitchen(userPrincipalProvider.get().getActiveKitchen());
            repository.save(item);
        } catch (DataAccessException e) {
            throw new RuntimeException("msg.internal.error", e);
        }
    }

    public void updateItem(final Item item) throws UserUnauthorizedException {
        try {
            if (item.getKitchen() == null) {
                item.setKitchen(userPrincipalProvider.get().getActiveKitchen());
            } else {
                checkIfUserHasItem(item);
            }

            repository.save(item);
        } catch (DataAccessException e) {
            throw new RuntimeException("msg.internal.error", e);
        }
    }

    private void checkIfUserHasItem(Item item) throws UserUnauthorizedException {
        Long kitchenId = item.getKitchen().getId();

        if (!userPrincipalProvider.getFresh().hasKitchen(kitchenId)) {
            throw new UserUnauthorizedException();
        }
    }

    public void deleteItem(final Long id) throws UserUnauthorizedException {
        checkIfUserHasItemById(id);

        try {
            repository.delete(id);
        } catch (DataAccessException e) {
            throw new RuntimeException("msg.internal.error", e);
        }
    }

    private void checkIfUserHasItemById(Long id) throws UserUnauthorizedException {
        Item item = repository.findById(id);
        checkIfUserHasItem(item);
    }

    public Item getItem(final Long id) throws UserUnauthorizedException {
        try {
            Item item = repository.findOne(id);

            if (item != null) {
                checkIfUserHasItem(item);
            }

            return item;
        } catch (DataAccessException e) {
            throw new RuntimeException("msg.internal.error", e);
        }
    }

    public Iterable<Item> getAllItemsForActiveKitchen() throws UserUnauthorizedException {
        checkIfUserOwnsHisActiveKitchen();

        try {
            return repository.findAllByKitchen(userPrincipalProvider.get().getActiveKitchen());
        } catch (DataAccessException e) {
            throw new RuntimeException("msg.internal.error", e);
        }
    }

    public Page<Item> getAllItemsForActiveKitchenPaginated(int page, int size) throws UserUnauthorizedException {
        checkIfUserOwnsHisActiveKitchen();

        try {
            Pageable pageable = new PageRequest(page, size);
            return repository.findAllByKitchenOrderByNameAsc(pageable, userPrincipalProvider.get().getActiveKitchen());
        } catch (DataAccessException e) {
            throw new RuntimeException("msg.internal.error", e);
        }
    }

}
