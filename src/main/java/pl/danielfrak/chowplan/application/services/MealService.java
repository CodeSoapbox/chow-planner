package pl.danielfrak.chowplan.application.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.danielfrak.chowplan.application.exceptions.UserUnauthorizedException;
import pl.danielfrak.chowplan.domain.grocerylist.meals.Meal;
import pl.danielfrak.chowplan.domain.grocerylist.meals.MealRepository;

@Service
public class MealService extends UserAwareEntityService {
    private final MealRepository mealRepository;

    @Autowired
    public MealService(
            UserPrincipalProvider userPrincipalProvider,
            MealRepository mealRepository
    ) {
        super(userPrincipalProvider);
        this.mealRepository = mealRepository;
    }

    public Iterable<Meal> getAllMealsForActiveKitchen() throws UserUnauthorizedException {
        checkIfUserOwnsHisActiveKitchen();

        try {
            return mealRepository.findAllByKitchen(userPrincipalProvider.get().getActiveKitchen());
        } catch (DataAccessException e) {
            throw new RuntimeException("msg.internal.error", e);
        }
    }

    public Page<Meal> getAllMealsForActiveKitchenPaginated(int page, int size) throws UserUnauthorizedException {
        checkIfUserOwnsHisActiveKitchen();

        try {
            Pageable pageable = new PageRequest(page, size);
            return mealRepository.findAllByKitchenOrderByNameAsc(pageable, userPrincipalProvider.get().getActiveKitchen());
        } catch (DataAccessException e) {
            throw new RuntimeException("msg.internal.error", e);
        }
    }

    public Meal getMeal(Long id) throws UserUnauthorizedException {
        try {
            Meal meal = mealRepository.findOne(id);

            if (meal != null) {
                checkIfUserHasMeal(meal);
            }

            return meal;
        } catch (DataAccessException e) {
            throw new RuntimeException("msg.internal.error", e);
        }
    }

    private void checkIfUserHasMeal(Meal meal) throws UserUnauthorizedException {
        Long kitchenId = meal.getKitchen().getId();

        if (!userPrincipalProvider.getFresh().hasKitchen(kitchenId)) {
            throw new UserUnauthorizedException();
        }
    }

    public void updateMeal(Meal meal) throws UserUnauthorizedException {
        try {
            if (meal.getKitchen() == null) {
                meal.setKitchen(userPrincipalProvider.get().getActiveKitchen());
            } else {
                checkIfUserHasMeal(meal);
            }

            mealRepository.save(meal);
        } catch (DataAccessException e) {
            throw new RuntimeException("msg.internal.error", e);
        }
    }

    public void addMeal(Meal meal) throws UserUnauthorizedException {
        try {
            meal.setKitchen(userPrincipalProvider.get().getActiveKitchen());
            mealRepository.save(meal);
        } catch (DataAccessException e) {
            throw new RuntimeException("msg.internal.error", e);
        }
    }

    public void deleteMeal(Long id) throws UserUnauthorizedException {
        checkIfUserHasMealById(id);

        try {
            mealRepository.delete(id);
        } catch (DataAccessException e) {
            throw new RuntimeException("msg.internal.error", e);
        }
    }

    private void checkIfUserHasMealById(Long id) throws UserUnauthorizedException {
        Meal meal = mealRepository.findById(id);
        checkIfUserHasMeal(meal);
    }
}
