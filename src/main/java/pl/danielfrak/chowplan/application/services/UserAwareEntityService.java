package pl.danielfrak.chowplan.application.services;

import pl.danielfrak.chowplan.application.authentication.UserPrincipal;
import pl.danielfrak.chowplan.application.exceptions.UserUnauthorizedException;
import pl.danielfrak.chowplan.domain.grocerylist.kitchens.Kitchen;

public abstract class UserAwareEntityService {
    protected UserPrincipalProvider userPrincipalProvider;

    public UserAwareEntityService(final UserPrincipalProvider userPrincipalProvider) {
        this.userPrincipalProvider = userPrincipalProvider;
    }

    protected void checkIfUserOwnsHisActiveKitchen() throws UserUnauthorizedException {
        UserPrincipal userPrincipal = userPrincipalProvider.getFresh();
        Kitchen activeKitchen = userPrincipalProvider.get().getActiveKitchen();
        if(activeKitchen == null) {
            throw new RuntimeException("msg.internal.error");
        }
        Long activeKitchenId = activeKitchen.getId();
        if(!userPrincipal.hasKitchen(activeKitchenId)) {
            throw new UserUnauthorizedException();
        }
    }
}
