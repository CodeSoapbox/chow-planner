package pl.danielfrak.chowplan.application.controllers;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.danielfrak.chowplan.application.messages.MessageList;
import pl.danielfrak.chowplan.application.messages.UserMessage;

import java.util.Map;

public abstract class BaseController {

    protected void addMessage(Map<String, Object> model, UserMessage message) {
        MessageList messages = (MessageList) model.get(ViewAttribute.MESSAGES);

        if (messages == null) {
            messages = new MessageList();
        }

        messages.addMessage(message);

        model.put(
                ViewAttribute.MESSAGES,
                messages
        );
    }

    protected String redirectWithMessage(RedirectAttributes redirectAttributes, String controllerPath, UserMessage message) {
        addRedirectMessage(
                redirectAttributes,
                message
        );

        return "redirect:" + controllerPath;
    }

    protected void addRedirectMessage(RedirectAttributes redirectAttributes, UserMessage message) {
        MessageList messages = (MessageList) redirectAttributes
                .getFlashAttributes()
                .get(ViewAttribute.MESSAGES);

        if (messages == null) {
            messages = new MessageList();
        }

        messages.addMessage(message);

        redirectAttributes.addFlashAttribute(
                ViewAttribute.MESSAGES,
                messages
        );
    }
}
