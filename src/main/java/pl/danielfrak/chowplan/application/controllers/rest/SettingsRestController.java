package pl.danielfrak.chowplan.application.controllers.rest;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.danielfrak.chowplan.application.controllers.ControllerPath;
import pl.danielfrak.chowplan.application.controllers.rest.request.ToggleHintsRequestBody;
import pl.danielfrak.chowplan.application.exceptions.UserUnauthorizedException;
import pl.danielfrak.chowplan.application.services.CookService;

import javax.servlet.http.HttpServletResponse;

@RestController
public class SettingsRestController {
    private final CookService cookService;

    public SettingsRestController(CookService cookService) {
        this.cookService = cookService;
    }

    @RequestMapping(
            value = ControllerPath.SETTINGS_TOGGLE_HINTS,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public Boolean toggleHints(@RequestBody(required = false) ToggleHintsRequestBody requestBody, HttpServletResponse response) {
        try {
            boolean enabled;

            if (requestBody == null) {
                enabled = !cookService.getHintsEnabled();
            } else {
                enabled = requestBody.isEnabled();
            }

            cookService.setHintsEnabled(enabled);

            response.setStatus(HttpServletResponse.SC_OK);
            return cookService.getHintsEnabled();
        } catch (UserUnauthorizedException e) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }

        return null;
    }
}
