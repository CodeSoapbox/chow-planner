package pl.danielfrak.chowplan.application.controllers.standard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.danielfrak.chowplan.application.controllers.BaseController;
import pl.danielfrak.chowplan.application.controllers.ControllerPath;
import pl.danielfrak.chowplan.application.controllers.ViewAttribute;
import pl.danielfrak.chowplan.application.controllers.ViewPath;
import pl.danielfrak.chowplan.application.exceptions.UserUnauthorizedException;
import pl.danielfrak.chowplan.application.messages.ErrorMessage;
import pl.danielfrak.chowplan.application.messages.MessageList;
import pl.danielfrak.chowplan.application.messages.SuccessMessage;
import pl.danielfrak.chowplan.application.services.ItemService;
import pl.danielfrak.chowplan.domain.grocerylist.items.Item;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class ItemsController extends BaseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ItemsController.class);
    private final ItemService itemService;

    @Autowired
    public ItemsController(ItemService itemService) {
        this.itemService = itemService;
    }

    @GetMapping(ControllerPath.ITEMS)
    public String items(
            final Map<String, Object> model,
            final @RequestParam(defaultValue = "1") int page,
            final @RequestParam(defaultValue = "10") int size,
            final @ModelAttribute(ViewAttribute.MESSAGES) MessageList messages,
            final RedirectAttributes redirectAttributes
    ) {
        model.put(ViewAttribute.MESSAGES, messages);

        Iterable<Item> items;
        try {
            Page<Item> itemPage = itemService.getAllItemsForActiveKitchenPaginated(page-1, size);
            items = itemPage.getContent();
            model.put(ViewAttribute.PAGES, itemPage.getTotalPages());
            model.put(ViewAttribute.CURRENT_PAGE, page);
        } catch (UserUnauthorizedException e) {
            return redirectWithMessage(
                    redirectAttributes,
                    ControllerPath.HOME,
                    new ErrorMessage("msg.error.unauthorized.action")
            );
        } catch (Exception e) {
            LOGGER.error("Can't access item list page");
            return redirectWithMessage(
                    redirectAttributes,
                    ControllerPath.HOME,
                    new ErrorMessage("msg.internal.error")
            );
        }

        model.put("items", items);

        return ViewPath.ITEMS;
    }

    @GetMapping(ControllerPath.ITEM_NEW)
    public String newItem(
            final Map<String, Object> model
    ) {
        model.put("item", new Item());

        return ViewPath.ITEM_EDIT;
    }

    @PostMapping(ControllerPath.ITEM_NEW)
    public String saveItem(
            final Map<String, Object> model,
            final RedirectAttributes redirectAttributes,
            @Valid @ModelAttribute("item") final Item item,
            BindingResult bindingResult
    ) {
        if (bindingResult.hasErrors()) {
            addMessage(model, new ErrorMessage("msg.validation.error"));
            return ViewPath.ITEM_EDIT;
        }

        try {
            itemService.addItem(item);

            addRedirectMessage(
                    redirectAttributes,
                    new SuccessMessage("msg.item.added")
            );
        } catch (UserUnauthorizedException e) {
            addMessage(model, new ErrorMessage("msg.error.unauthorized.action"));
        } catch (Exception e) {
            LOGGER.error("Can't save new item", e);
            addMessage(model, new ErrorMessage("msg.internal.error"));
            return ViewPath.ITEM_EDIT;
        }

        return "redirect:" + ControllerPath.ITEMS;
    }

    @GetMapping(ControllerPath.ITEM_EDIT + "/{id}")
    public String editItem(
            final Map<String, Object> model,
            final RedirectAttributes redirectAttributes,
            @PathVariable final Long id
    ) {
        try {
            return tryToEditItem(model, redirectAttributes, id);
        } catch (UserUnauthorizedException e) {
            return userIsUnauthorized(redirectAttributes);
        } catch (Exception e) {
            LOGGER.error("Can't access edit item page", e);
            return redirectWithMessage(
                    redirectAttributes,
                    ControllerPath.ITEMS,
                    new ErrorMessage("msg.internal.error")
            );
        }
    }

    private String userIsUnauthorized(RedirectAttributes redirectAttributes) {
        addRedirectMessage(
                redirectAttributes,
                new ErrorMessage("msg.error.unauthorized.action")
        );
        return "redirect:" + ControllerPath.ITEMS;
    }

    private String tryToEditItem(Map<String, Object> model, RedirectAttributes redirectAttributes, @PathVariable Long id) throws UserUnauthorizedException {
        Item item = itemService.getItem(id);

        if (item == null) {
            addRedirectMessage(
                    redirectAttributes,
                    new ErrorMessage("msg.no.such.item")
            );
            return "redirect:" + ControllerPath.ITEMS;
        }

        model.put("item", item);

        return ViewPath.ITEM_EDIT;
    }

    @PostMapping(ControllerPath.ITEM_EDIT + "/{id}")
    public String updateItem(
            final Map<String, Object> model,
            final RedirectAttributes redirectAttributes,
            @Valid @ModelAttribute("item") final Item item,
            BindingResult bindingResult,
            @PathVariable Long id
    ) {
        if (bindingResult.hasErrors()) {
            addMessage(model, new ErrorMessage("msg.validation.error"));
            return ViewPath.ITEM_EDIT;
        }

        item.setId(id);

        try {
            itemService.updateItem(item);

            addRedirectMessage(
                    redirectAttributes,
                    new SuccessMessage("msg.item.edited")
            );
        } catch (UserUnauthorizedException e) {
            addMessage(model, new ErrorMessage("msg.error.unauthorized.action"));
            return ViewPath.ITEM_EDIT;
        } catch (Exception e) {
            LOGGER.error("Can't update item: {}", item, e);
            addMessage(model, new ErrorMessage("msg.internal.error"));
            return ViewPath.ITEM_EDIT;
        }

        return "redirect:" + ControllerPath.ITEMS;
    }

    @GetMapping(ControllerPath.ITEM_DELETE + "/{id}")
    public String deleteItem(
            final RedirectAttributes redirectAttributes,
            @PathVariable final Long id
    ) {
        try {
            itemService.deleteItem(id);

            addRedirectMessage(
                    redirectAttributes,
                    new SuccessMessage("msg.item.deleted")
            );
        } catch (UserUnauthorizedException e) {
            addRedirectMessage(
                    redirectAttributes,
                    new ErrorMessage("msg.error.unauthorized.action")
            );
        } catch (Exception e) {
            LOGGER.error("Can't delete item (id: {})", id, e);
            addRedirectMessage(
                    redirectAttributes,
                    new ErrorMessage("msg.internal.error")
            );
        }

        return "redirect:" + ControllerPath.ITEMS;
    }
}
