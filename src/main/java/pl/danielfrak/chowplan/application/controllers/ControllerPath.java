package pl.danielfrak.chowplan.application.controllers;

public class ControllerPath {
    public static final String HOME = "/";
    public static final String ITEM_NEW = "/item/new";
    public static final String ITEM_EDIT = "/item/edit";
    public static final String ITEM_DELETE = "/item/delete";
    public static final String ITEMS = "/items";
    public static final String KITCHENS = "/kitchens";
    public static final String KITCHEN_NEW = "/kitchen/new";
    public static final String KITCHEN_EDIT = "/kitchen/edit";
    public static final String KITCHEN_DELETE = "/kitchen/delete";
    public static final String KITCHEN_CHOOSE = "/kitchen/choose";
    public static final String MEALS = "/meals";
    public static final String MEAL_NEW = "/meal/new";
    public static final String MEAL_EDIT = "/meal/edit";
    public static final String MEAL_DELETE = "/meal/delete";
    public static final String SIGN_IN = "/signin";
    public static final String SETTINGS = "/settings";
    public static final String SETTINGS_TOGGLE_HINTS = "/settings/hints/toggle";
}
