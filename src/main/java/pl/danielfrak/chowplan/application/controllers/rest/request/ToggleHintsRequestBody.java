package pl.danielfrak.chowplan.application.controllers.rest.request;

public class ToggleHintsRequestBody {
    private boolean enabled;

    public boolean isEnabled() {
        return enabled;
    }
}
