package pl.danielfrak.chowplan.application.controllers.standard;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.danielfrak.chowplan.application.controllers.BaseController;
import pl.danielfrak.chowplan.application.controllers.ControllerPath;
import pl.danielfrak.chowplan.application.controllers.ViewPath;

@Controller
public class SettingsController extends BaseController {
    @RequestMapping(ControllerPath.SETTINGS)
    public String settings() {
        return ViewPath.SETTINGS;
    }
}
