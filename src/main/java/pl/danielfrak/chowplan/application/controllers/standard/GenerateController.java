package pl.danielfrak.chowplan.application.controllers.standard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.danielfrak.chowplan.application.controllers.BaseController;
import pl.danielfrak.chowplan.application.controllers.ControllerPath;
import pl.danielfrak.chowplan.application.controllers.ViewPath;
import pl.danielfrak.chowplan.application.controllers.standard.formdata.GroceryListGenerationFormData;
import pl.danielfrak.chowplan.application.exceptions.UserUnauthorizedException;
import pl.danielfrak.chowplan.application.messages.ErrorMessage;
import pl.danielfrak.chowplan.application.messages.SuccessMessage;
import pl.danielfrak.chowplan.application.services.MealService;
import pl.danielfrak.chowplan.domain.grocerylist.grocerylist.GroceryListGenerator;
import pl.danielfrak.chowplan.domain.grocerylist.meals.ChosenMeal;
import pl.danielfrak.chowplan.domain.grocerylist.meals.Ingredient;
import pl.danielfrak.chowplan.domain.grocerylist.meals.Meal;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.StreamSupport;

@Controller
public class GenerateController extends BaseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(GenerateController.class);
    private final MealService mealService;

    @Autowired
    public GenerateController(MealService mealService) {
        this.mealService = mealService;
    }

    @RequestMapping("/")
    public String generate(
            Map<String, Object> model,
            final RedirectAttributes redirectAttributes
    ) {
        try {
            model.put("meals", mealService.getAllMealsForActiveKitchen());
            model.put("formData", new GroceryListGenerationFormData());

            return ViewPath.GENERATE;
        } catch (UserUnauthorizedException e) {
            return userIsUnauthorized(redirectAttributes);
        }
    }

    private String userIsUnauthorized(RedirectAttributes redirectAttributes) {
        return redirectWithMessage(
                redirectAttributes,
                ControllerPath.HOME,
                new ErrorMessage("msg.error.unauthorized.action")
        );
    }

    @PostMapping("/")
    public String createGroceryListFromMeals(
            Map<String, Object> model,
            final RedirectAttributes redirectAttributes,
            @Valid @ModelAttribute("formData") final GroceryListGenerationFormData formData,
            BindingResult bindingResult
    ) {
        try {
            model.put("meals", mealService.getAllMealsForActiveKitchen());

            if (bindingResult.hasErrors()) {
                addMessage(model, new ErrorMessage("msg.validation.error"));
                return ViewPath.GENERATE;
            }

            GroceryListGenerator groceryListGenerator = new GroceryListGenerator();
            List<Ingredient> groceryList = groceryListGenerator.fromMeals(getChosenMealsWithIngredients(formData));
            model.put("groceryList", groceryList);
            addMessage(model, new SuccessMessage("msg.grocery.list.generated"));
        } catch (UserUnauthorizedException e) {
            addMessage(model, new ErrorMessage("msg.error.unauthorized.action"));
        } catch (Exception e) {
            LOGGER.error("Can't generate grocery list from meals: {}", formData, e);
            addMessage(model, new ErrorMessage("msg.internal.error"));
            return ViewPath.GENERATE;
        }

        return ViewPath.GROCERY_LIST;
    }

    private List<ChosenMeal> getChosenMealsWithIngredients(GroceryListGenerationFormData formData)
            throws UserUnauthorizedException {
        List<ChosenMeal> meals = formData.getChosenMeals();
        Iterable<Meal> freshMeals = mealService.getAllMealsForActiveKitchen();
        meals.forEach(meal -> fillIngredientsOrRemove(meals, freshMeals, meal));

        return meals;
    }

    private void fillIngredientsOrRemove(List<ChosenMeal> meals, Iterable<Meal> freshMeals, ChosenMeal meal) {
        Optional<Meal> freshMeal = getCorrespondingFreshMeal(freshMeals, meal);
        if (freshMeal.isPresent()) {
            meal.setIngredients(freshMeal.get().getIngredients());
        } else {
            meals.remove(meal);
        }
    }

    private Optional<Meal> getCorrespondingFreshMeal(Iterable<Meal> freshMeals, ChosenMeal meal) {
        return StreamSupport.stream(freshMeals.spliterator(), false)
                .filter(freshMealPred -> freshMealPred.getId().equals(meal.getId())).findFirst();
    }
}
