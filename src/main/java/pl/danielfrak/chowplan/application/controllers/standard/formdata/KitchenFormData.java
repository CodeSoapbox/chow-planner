package pl.danielfrak.chowplan.application.controllers.standard.formdata;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

public class KitchenFormData {
    @NotNull
    @NotEmpty
    private String name;

    protected KitchenFormData() {
    }

    public KitchenFormData(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
