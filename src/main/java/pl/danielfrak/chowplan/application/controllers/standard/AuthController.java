package pl.danielfrak.chowplan.application.controllers.standard;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.danielfrak.chowplan.application.controllers.BaseController;
import pl.danielfrak.chowplan.application.controllers.ControllerPath;
import pl.danielfrak.chowplan.application.messages.ErrorMessage;
import pl.danielfrak.chowplan.application.messages.SuccessMessage;

import java.util.Map;

@Controller
public class AuthController extends BaseController {
    @RequestMapping(ControllerPath.SIGN_IN)
    public String signIn(
            Map<String, Object> model,
            @RequestParam(required = false) String error,
            @RequestParam(required = false) String logout
    ) {
        if (error != null) {
            addMessage(model, new ErrorMessage("msg.signin.error"));
        }

        if (logout != null) {
            addMessage(model, new SuccessMessage("msg.logout"));
        }

        return "views/sign_in";
    }

    @GetMapping("/signup")
    public String signUp(
            Map<String, Object> model
    ) {
        return "views/sign_up";
    }

    @RequestMapping("/logout")
    public String logOut(
            Map<String, Object> model
    ) {
        return "views/sign_in";
    }
}
