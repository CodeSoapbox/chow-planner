package pl.danielfrak.chowplan.application.controllers.standard.formdata;

import org.hibernate.validator.constraints.NotEmpty;
import pl.danielfrak.chowplan.domain.grocerylist.meals.ChosenMeal;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class GroceryListGenerationFormData {
    @NotNull
    @NotEmpty
    private List<ChosenMeal> chosenMeals;

    public GroceryListGenerationFormData() {
        chosenMeals = new ArrayList<>();
    }

    public List<ChosenMeal> getChosenMeals() {
        return chosenMeals;
    }

    public void setChosenMeals(List<ChosenMeal> chosenMeals) {
        this.chosenMeals = chosenMeals;
    }
}
