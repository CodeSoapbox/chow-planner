package pl.danielfrak.chowplan.application.controllers;

public class ViewPath {
    public static final String ITEM_EDIT = "views/item/edit";
    public static final String ITEMS = "views/items";
    public static final String KITCHEN_EDIT = "views/kitchen/edit";
    public static final String MEALS = "views/meals";
    public static final String MEAL_EDIT = "views/meal/edit";
    public static final String GENERATE = "views/generate";
    public static final String GROCERY_LIST = "views/grocerylist";
    public static final String SETTINGS = "views/settings";
}
