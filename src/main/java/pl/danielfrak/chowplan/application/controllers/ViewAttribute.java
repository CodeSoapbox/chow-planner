package pl.danielfrak.chowplan.application.controllers;

public class ViewAttribute {
    public static final String MESSAGES = "messages";
    public static final String PAGES = "pages";
    public static final String CURRENT_PAGE = "currentPage";
}
