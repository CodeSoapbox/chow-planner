package pl.danielfrak.chowplan.application.controllers.standard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.danielfrak.chowplan.application.controllers.BaseController;
import pl.danielfrak.chowplan.application.controllers.ControllerPath;
import pl.danielfrak.chowplan.application.controllers.ViewPath;
import pl.danielfrak.chowplan.application.controllers.standard.formdata.KitchenFormData;
import pl.danielfrak.chowplan.application.exceptions.NoSuchKitchenException;
import pl.danielfrak.chowplan.application.exceptions.UserUnauthorizedException;
import pl.danielfrak.chowplan.application.messages.ErrorMessage;
import pl.danielfrak.chowplan.application.messages.SuccessMessage;
import pl.danielfrak.chowplan.application.services.KitchenService;
import pl.danielfrak.chowplan.domain.grocerylist.kitchens.Kitchen;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class KitchensController extends BaseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(KitchensController.class);
    private final KitchenService kitchenService;

    @Autowired
    public KitchensController(KitchenService kitchenService) {
        this.kitchenService = kitchenService;
    }

    @GetMapping(ControllerPath.KITCHENS)
    public String kitchens(
            Map<String, Object> model,
            RedirectAttributes redirectAttributes
    ) {
        try {
            Iterable<Kitchen> kitchens = kitchenService.getKitchens();

            model.put("kitchens", kitchens);
            model.put("chosenKitchen", kitchenService.getActiveKitchen());
        } catch (UserUnauthorizedException e) {
            redirectWithMessage(
                    redirectAttributes,
                    ControllerPath.SIGN_IN,
                    new ErrorMessage("msg.error.unauthorized.page")
            );
        } catch (Exception e) {
            LOGGER.error("Can't access kitchen list page");
            return redirectWithMessage(
                    redirectAttributes,
                    ControllerPath.HOME,
                    new ErrorMessage("msg.internal.error")
            );
        }

        return "views/kitchens";
    }

    @GetMapping(ControllerPath.KITCHEN_NEW)
    public String newKitchen(Map<String, Object> model) {
        model.put("kitchen", new Kitchen());
        return "views/kitchen/edit";
    }

    @PostMapping(ControllerPath.KITCHEN_NEW)
    public String saveKitchen(
            final Map<String, Object> model,
            final RedirectAttributes redirectAttributes,
            @Valid @ModelAttribute("kitchen") final Kitchen kitchen,
            BindingResult bindingResult
    ) {
        if (bindingResult.hasErrors()) {
            addMessage(model, new ErrorMessage("msg.validation.error"));
            return ViewPath.KITCHEN_EDIT;
        }

        try {
            kitchenService.addKitchen(kitchen);

            addRedirectMessage(
                    redirectAttributes,
                    new SuccessMessage("msg.kitchen.added")
            );
        } catch (UserUnauthorizedException e) {
            addMessage(model, new ErrorMessage("msg.error.unauthorized.action"));
        } catch (Exception e) {
            LOGGER.error("Can't save new kitchen", e);
            addMessage(model, new ErrorMessage("msg.internal.error"));
            return ViewPath.KITCHEN_EDIT;
        }

        return "redirect:" + ControllerPath.KITCHENS;
    }

    @GetMapping(ControllerPath.KITCHEN_EDIT + "/{id}")
    public String editKitchen(
            final Map<String, Object> model,
            final RedirectAttributes redirectAttributes,
            @PathVariable final int id
    ) {
        try {
            model.put("kitchen", kitchenService.getKitchen(new Long(id)));
            return ViewPath.KITCHEN_EDIT;
        } catch (UserUnauthorizedException e) {
            return redirectWithMessage(
                    redirectAttributes,
                    ControllerPath.KITCHENS,
                    new ErrorMessage("msg.error.unauthorized.page")
            );
        } catch (Exception e) {
            LOGGER.error("Can't edit kitchen (id: " + id + ")", e);
            return redirectWithMessage(
                    redirectAttributes,
                    ControllerPath.KITCHENS,
                    new ErrorMessage("msg.internal.error")
            );
        }
    }

    @PostMapping(ControllerPath.KITCHEN_EDIT + "/{id}")
    public String updateKitchen(
            final Map<String, Object> model,
            final RedirectAttributes redirectAttributes,
            @Valid @ModelAttribute("kitchen") final KitchenFormData kitchenFormData,
            BindingResult bindingResult,
            @PathVariable Long id
    ) {
        if (bindingResult.hasErrors()) {
            addMessage(model, new ErrorMessage("msg.validation.error"));
            return ViewPath.KITCHEN_EDIT;
        }

        try {
            Kitchen kitchen = kitchenService.getKitchen(id);
            kitchen.setName(kitchenFormData.getName());

            kitchenService.updateKitchen(kitchen);

            addRedirectMessage(
                    redirectAttributes,
                    new SuccessMessage("msg.kitchen.edited")
            );

            return "redirect:" + ControllerPath.KITCHENS;
        } catch (NoSuchKitchenException | UserUnauthorizedException e) {
            return redirectWithMessage(
                    redirectAttributes,
                    ControllerPath.KITCHENS,
                    new ErrorMessage("msg.error.unauthorized.action")
            );
        } catch (Exception e) {
            LOGGER.error("Can't update kitchen (id: " + id + ")", e);
            addMessage(model, new ErrorMessage("msg.internal.error"));
            return ViewPath.KITCHEN_EDIT;
        }
    }

    @GetMapping(ControllerPath.KITCHEN_DELETE + "/{id}")
    public String deleteKitchen(
            final RedirectAttributes redirectAttributes,
            @PathVariable final Long id
    ) {
        try {
            kitchenService.deleteKitchen(id);

            return redirectWithMessage(
                    redirectAttributes,
                    ControllerPath.KITCHENS,
                    new SuccessMessage("msg.kitchen.deleted")
            );
        } catch (UserUnauthorizedException e) {
            return redirectWithMessage(
                    redirectAttributes,
                    ControllerPath.KITCHENS,
                    new ErrorMessage("msg.error.unauthorized.action")
            );
        } catch (Exception e) {
            LOGGER.error("Can't delete kitchen (id: " + id + ")", e);
            return redirectWithMessage(
                    redirectAttributes,
                    ControllerPath.KITCHENS,
                    new ErrorMessage("msg.internal.error")
            );
        }
    }

    @GetMapping(ControllerPath.KITCHEN_CHOOSE + "/{id}")
    public String setActiveKitchen(
            Map<String, Object> model,
            final RedirectAttributes redirectAttributes,
            @PathVariable int id
    ) {
        try {
            kitchenService.setActiveKitchen(new Long(id));
        } catch (UserUnauthorizedException e) {
            return redirectWithMessage(
                    redirectAttributes,
                    ControllerPath.SIGN_IN,
                    new ErrorMessage("msg.error.unauthorized.page")
            );
        } catch (Exception e) {
            LOGGER.error("Can't set active kitchen (id: " + id + ")", e);
            return redirectWithMessage(
                    redirectAttributes,
                    ControllerPath.KITCHENS,
                    new ErrorMessage("msg.internal.error")
            );
        }

        return redirectWithMessage(
                redirectAttributes,
                ControllerPath.KITCHENS,
                new SuccessMessage("msg.active.kitchen.set")
        );
    }
}
