package pl.danielfrak.chowplan.application.controllers.standard;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.danielfrak.chowplan.application.controllers.*;
import pl.danielfrak.chowplan.application.exceptions.UserUnauthorizedException;
import pl.danielfrak.chowplan.application.messages.ErrorMessage;
import pl.danielfrak.chowplan.application.messages.MessageList;
import pl.danielfrak.chowplan.application.messages.SuccessMessage;
import pl.danielfrak.chowplan.application.services.ItemService;
import pl.danielfrak.chowplan.application.services.MealService;
import pl.danielfrak.chowplan.domain.grocerylist.meals.Meal;

import javax.validation.Valid;
import java.util.Map;

@Controller
public class MealsController extends BaseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(MealsController.class);
    private final MealService mealService;
    private final ItemService itemService;

    @Autowired
    public MealsController(MealService mealService, ItemService itemService) {
        this.mealService = mealService;
        this.itemService = itemService;
    }

    @RequestMapping(ControllerPath.MEALS)
    public String meals(
            final Map<String, Object> model,
            final @RequestParam(defaultValue = "1") int page,
            final @RequestParam(defaultValue = "10") int size,
            final @ModelAttribute(ViewAttribute.MESSAGES) MessageList messages,
            final RedirectAttributes redirectAttributes
    ) {
        model.put(ViewAttribute.MESSAGES, messages);

        try {
            Page<Meal> mealPage = mealService.getAllMealsForActiveKitchenPaginated(page-1, size);
            Iterable<Meal> meals = mealPage.getContent();
            model.put(ViewAttribute.PAGES, mealPage.getTotalPages());
            model.put(ViewAttribute.CURRENT_PAGE, page);

            model.put("meals", meals);

            return ViewPath.MEALS;
        } catch (UserUnauthorizedException e) {
            return userIsUnauthorized(redirectAttributes);
        } catch (Exception e) {
            LOGGER.error("Can't access meal list page", e);
            return redirectWithMessage(
                    redirectAttributes,
                    ControllerPath.HOME,
                    new ErrorMessage("msg.internal.error")
            );
        }
    }

    private String userIsUnauthorized(RedirectAttributes redirectAttributes) {
        return redirectWithMessage(
                redirectAttributes,
                ControllerPath.HOME,
                new ErrorMessage("msg.error.unauthorized.action")
        );
    }

    @GetMapping(ControllerPath.MEAL_NEW)
    public String newMeal(
            Map<String, Object> model,
            RedirectAttributes redirectAttributes
    ) {
        model.put("meal", new Meal());

        try {
            model.put("items", itemService.getAllItemsForActiveKitchen());
        } catch (UserUnauthorizedException e) {
            return userIsUnauthorized(redirectAttributes);
        } catch (Exception e) {
            LOGGER.error("Can't access new meal page", e);
            return redirectWithMessage(
                    redirectAttributes,
                    ControllerPath.MEALS,
                    new ErrorMessage("msg.internal.error")
            );
        }

        return "views/meal/edit";
    }

    @PostMapping(ControllerPath.MEAL_NEW)
    public String saveMeal(
            final Map<String, Object> model,
            final RedirectAttributes redirectAttributes,
            @Valid @ModelAttribute("meal") final Meal meal,
            BindingResult bindingResult
    ) {
        if (bindingResult.hasErrors()) {
            addMessage(model, new ErrorMessage("msg.validation.error"));
            return ViewPath.MEAL_EDIT;
        }

        try {
            mealService.addMeal(meal);

            addRedirectMessage(
                    redirectAttributes,
                    new SuccessMessage("msg.meal.added")
            );
        } catch (UserUnauthorizedException e) {
            addMessage(model, new ErrorMessage("msg.error.unauthorized.action"));
        } catch (Exception e) {
            LOGGER.error("Can't save new meal: {}", meal, e);
            addMessage(model, new ErrorMessage("msg.internal.error"));
            return ViewPath.ITEM_EDIT;
        }

        return "redirect:" + ControllerPath.MEALS;
    }

    @GetMapping(ControllerPath.MEAL_EDIT + "/{id}")
    public String editMeal(
            Map<String, Object> model,
            final RedirectAttributes redirectAttributes,
            @PathVariable("id") Long id
    ) {
        try {
            return tryToEditMeal(model, redirectAttributes, id);
        } catch (UserUnauthorizedException e) {
            return userIsUnauthorized(redirectAttributes);
        } catch (Exception e) {
            LOGGER.error("Can't access edit meal page", e);
            return redirectWithMessage(
                    redirectAttributes,
                    ControllerPath.MEALS,
                    new ErrorMessage("msg.internal.error")
            );
        }
    }

    private String tryToEditMeal(
            Map<String, Object> model,
            RedirectAttributes redirectAttributes,
            Long id
    ) throws UserUnauthorizedException {

        Meal meal = mealService.getMeal(id);

        if (meal == null) {
            addRedirectMessage(
                    redirectAttributes,
                    new ErrorMessage("msg.no.such.meal")
            );
            return "redirect:" + ControllerPath.MEALS;
        }

        model.put("meal", meal);
        model.put("items", itemService.getAllItemsForActiveKitchen());
        return ViewPath.MEAL_EDIT;
    }

    @PostMapping(ControllerPath.MEAL_EDIT + "/{id}")
    public String updateMeal(
            final Map<String, Object> model,
            final RedirectAttributes redirectAttributes,
            @Valid @ModelAttribute("meal") final Meal meal,
            BindingResult bindingResult,
            @PathVariable Long id
    ) {

        try {
            model.put("items", itemService.getAllItemsForActiveKitchen());

            if (bindingResult.hasErrors()) {
                addMessage(model, new ErrorMessage("msg.validation.error"));
                return ViewPath.MEAL_EDIT;
            }

            meal.setId(id);
            mealService.updateMeal(meal);

            addRedirectMessage(
                    redirectAttributes,
                    new SuccessMessage("msg.meal.edited")
            );
        } catch (UserUnauthorizedException e) {
            LOGGER.error("Can't edit meal", e);
            addMessage(model, new ErrorMessage("msg.error.unauthorized.action"));
            return ViewPath.MEAL_EDIT;
        } catch (Exception e) {
            LOGGER.error("Can't update meal: {}", meal, e);
            return redirectWithMessage(
                    redirectAttributes,
                    ControllerPath.MEALS,
                    new ErrorMessage("msg.internal.error")
            );
        }

        return "redirect:" + ControllerPath.MEALS;
    }

    @GetMapping(ControllerPath.MEAL_DELETE + "/{id}")
    public String deleteMeal(
            final RedirectAttributes redirectAttributes,
            @PathVariable final Long id
    ) {
        try {
            mealService.deleteMeal(id);

            addRedirectMessage(
                    redirectAttributes,
                    new SuccessMessage("msg.meal.deleted")
            );
        } catch (UserUnauthorizedException e) {
            addRedirectMessage(
                    redirectAttributes,
                    new ErrorMessage("msg.error.unauthorized.action")
            );
        } catch (Exception e) {
            LOGGER.error("Can't delete meal (id: {})", id, e);
            addRedirectMessage(
                    redirectAttributes,
                    new ErrorMessage("msg.internal.error")
            );
        }

        return "redirect:" + ControllerPath.MEALS;
    }
}
