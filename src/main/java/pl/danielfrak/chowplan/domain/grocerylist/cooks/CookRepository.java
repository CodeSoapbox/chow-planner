package pl.danielfrak.chowplan.domain.grocerylist.cooks;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface CookRepository extends CrudRepository<Cook, Long> {
    @Query("SELECT c FROM Cook c JOIN FETCH c.kitchens WHERE c.username = (:username)")
    Cook findByUsernameWithKitchens(@Param("username") String username);

    @Query("SELECT c FROM Cook c JOIN FETCH c.kitchens WHERE c.id = (:id)")
    Cook findOneWithKitchens(@Param("id") Long id);
}
