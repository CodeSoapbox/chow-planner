package pl.danielfrak.chowplan.domain.grocerylist.items;

import org.hibernate.validator.constraints.NotEmpty;
import pl.danielfrak.chowplan.domain.grocerylist.kitchens.Kitchen;
import pl.danielfrak.chowplan.domain.grocerylist.meals.Ingredient;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;

@Entity
public class Item {
    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @NotEmpty
    private String name;

    @NotNull
    private String unit;

    @OneToMany(mappedBy = "item")
    private List<Ingredient> ingredients;

    @ManyToOne
    private Kitchen kitchen;

    public Item() {
    }

    public Item(Long id, String name, String unit, Kitchen kitchen) {
        this.id = id;
        this.name = name;
        this.unit = unit;
        this.kitchen = kitchen;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Kitchen getKitchen() {
        return kitchen;
    }

    public void setKitchen(Kitchen kitchen) {
        this.kitchen = kitchen;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, unit, ingredients, kitchen);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Item)) return false;
        Item item = (Item) o;
        return Objects.equals(id, item.id) &&
                Objects.equals(name, item.name) &&
                Objects.equals(unit, item.unit) &&
                Objects.equals(ingredients, item.ingredients) &&
                Objects.equals(kitchen, item.kitchen);
    }
}
