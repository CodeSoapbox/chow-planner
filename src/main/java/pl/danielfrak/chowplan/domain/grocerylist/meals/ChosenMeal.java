package pl.danielfrak.chowplan.domain.grocerylist.meals;

import pl.danielfrak.chowplan.domain.grocerylist.kitchens.Kitchen;

public class ChosenMeal extends Meal {
    int quantity;

    public ChosenMeal() {
    }

    public ChosenMeal(Long id, String name, int quantity, Kitchen kitchen) {
        super(id, name, kitchen);

        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
