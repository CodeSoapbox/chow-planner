package pl.danielfrak.chowplan.domain.grocerylist.items;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import pl.danielfrak.chowplan.domain.grocerylist.kitchens.Kitchen;

public interface ItemRepository extends PagingAndSortingRepository<Item, Long> {
    Item findById(Long id);

    Item findByNameAndKitchen(String name, Kitchen kitchen);

    Iterable<Item> findAllByKitchen(Kitchen kitchen);

    Page<Item> findAllByKitchenOrderByNameAsc(Pageable pageable, Kitchen kitchen);
}
