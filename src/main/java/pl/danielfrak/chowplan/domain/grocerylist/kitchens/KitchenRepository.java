package pl.danielfrak.chowplan.domain.grocerylist.kitchens;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface KitchenRepository extends CrudRepository<Kitchen, Long> {
    Iterable<Kitchen> findByCooksId(Long id);

    @Query("SELECT k FROM Kitchen k JOIN FETCH k.cooks WHERE k.id = (:id)")
    Kitchen findOneWithCooks(@Param("id") Long id);
}
