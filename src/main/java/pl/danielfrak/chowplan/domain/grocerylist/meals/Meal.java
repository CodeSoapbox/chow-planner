package pl.danielfrak.chowplan.domain.grocerylist.meals;

import org.hibernate.validator.constraints.NotEmpty;
import pl.danielfrak.chowplan.domain.grocerylist.kitchens.Kitchen;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
public class Meal {
    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty
    @NotNull
    private String name;

    @Column(columnDefinition="TEXT")
    @NotNull
    private String recipe;

    @ManyToOne
    private Kitchen kitchen;

    @OneToMany(cascade = CascadeType.ALL)
    @NotEmpty
    @NotNull
    private List<Ingredient> ingredients;

    public Meal() {
    }

    public Meal(Long id, String name, Kitchen kitchen) {
        this.id = id;
        this.name = name;
        this.kitchen = kitchen;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRecipe() {
        return recipe;
    }

    public void setRecipe(String recipe) {
        this.recipe = recipe;
    }

    public Kitchen getKitchen() {
        return kitchen;
    }

    public void setKitchen(Kitchen kitchen) {
        this.kitchen = kitchen;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }
}
