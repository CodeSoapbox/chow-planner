package pl.danielfrak.chowplan.domain.grocerylist.grocerylist;

import pl.danielfrak.chowplan.domain.grocerylist.meals.ChosenMeal;
import pl.danielfrak.chowplan.domain.grocerylist.meals.Ingredient;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class GroceryListGenerator {
    public List<Ingredient> fromMeals(List<ChosenMeal> meals) {
        List<Ingredient> groceryList = new ArrayList<>();
        meals.forEach(meal -> addIngredientsFromMeal(groceryList, meal));
        return groceryList;
    }

    private void addIngredientsFromMeal(List<Ingredient> groceryList, ChosenMeal meal) {
        meal.getIngredients().forEach(ingredient -> addOrUpdateGroceryItem(groceryList, ingredient, meal.getQuantity()));
    }

    private void addOrUpdateGroceryItem(List<Ingredient> groceryList, Ingredient ingredient, int mealQuantity) {
        Optional<Ingredient> groceryItem = groceryList
                .stream()
                .filter(groceryItemPred -> groceryItemPred.getItem().getId().equals(ingredient.getItem().getId()))
                .findFirst();
        if (groceryItem.isPresent()) {
            groceryItem.get().setQuantity(groceryItem.get().getQuantity() + (ingredient.getQuantity() * mealQuantity));
        } else {
            ingredient.setQuantity(ingredient.getQuantity() * mealQuantity);
            groceryList.add(ingredient);
        }
    }
}
