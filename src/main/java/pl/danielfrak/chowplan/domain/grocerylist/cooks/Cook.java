package pl.danielfrak.chowplan.domain.grocerylist.cooks;

import org.hibernate.validator.constraints.NotEmpty;
import pl.danielfrak.chowplan.domain.grocerylist.kitchens.Kitchen;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Cook {
    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @NotEmpty
    private String username;

    @NotNull
    @NotEmpty
    private String password;

    @ManyToOne
    private Kitchen activeKitchen;

    @ManyToMany(mappedBy = "cooks")
    private Set<Kitchen> kitchens;

    private boolean showHints = true;

    public Cook() {
        kitchens = new HashSet<>();
    }

    public Cook(Long id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
        kitchens = new HashSet<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Kitchen getActiveKitchen() {
        return activeKitchen;
    }

    public void setActiveKitchen(Kitchen activeKitchen) {
        this.activeKitchen = activeKitchen;
    }

    public Set<Kitchen> getKitchens() {
        return kitchens;
    }

    public void setKitchens(Set<Kitchen> kitchens) {
        this.kitchens = kitchens;
    }

    public boolean getShowHints() {
        return showHints;
    }

    public void setShowHints(boolean showHints) {
        this.showHints = showHints;
    }
}
