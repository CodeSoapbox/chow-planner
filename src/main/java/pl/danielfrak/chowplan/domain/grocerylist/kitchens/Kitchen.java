package pl.danielfrak.chowplan.domain.grocerylist.kitchens;

import org.hibernate.validator.constraints.NotEmpty;
import pl.danielfrak.chowplan.domain.grocerylist.cooks.Cook;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Kitchen {
    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @NotEmpty
    private String name;

    @ManyToMany
    private Set<Cook> cooks;

    public Kitchen() {
    }

    public Kitchen(
            Long id,
            String name,
            Cook cook
    ) {
        this.id = id;
        this.name = name;
        cooks = new HashSet<>();
        cooks.add(cook);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Cook> getCooks() {
        return cooks;
    }

    public void setCooks(Set<Cook> cooks) {
        this.cooks = cooks;
    }
}
