package pl.danielfrak.chowplan.domain.grocerylist.meals;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import pl.danielfrak.chowplan.domain.grocerylist.kitchens.Kitchen;

public interface MealRepository extends PagingAndSortingRepository<Meal, Long> {
    Meal findById(Long id);

    Iterable<Meal> findAllByKitchen(Kitchen kitchen);

    Page<Meal> findAllByKitchenOrderByNameAsc(Pageable pageable, Kitchen kitchen);
}
