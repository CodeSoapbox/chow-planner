(function(){
    "use strict";

    $(document)
        .ready(function() {
            translate['add']({
                'toast.hints.enabled': 'Page hints were enabled',
                'toast.hints.disabled': 'Page hints were disabled',
                'toast.hints.error': 'An error occurred while trying to toggle page hints'
            });
        })
    ;
})();