// grab our packages
var gulp       = require('gulp'),
    jshint     = require('gulp-jshint'),
    sass       = require('gulp-sass'),
    merge       = require('merge-stream'),
    concat       = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    minify = require('gulp-minify-css'),
    babel = require('gulp-babel'),
    sourcemaps = require('gulp-sourcemaps');

var semantic       = require('./semantic/tasks/build');

//gulp.task('default', ['semantic', 'dev']);
gulp.task('default', ['dev']);
gulp.task('dev', ['css', 'js', 'locale']);

gulp.task('css', function() {
    const cssStream = gulp.src('semantic/compiled/semantic.css')
        .pipe(sourcemaps.init())  // Process the original sources
        .pipe(minify());

    const sassStream = gulp.src(['toastr/**/*.scss', 'sass/**/*.scss'])
        .pipe(sourcemaps.init())  // Process the original sources
        .pipe(sass()) // Add the map to modified source.
        .pipe(minify());

    return merge(cssStream, sassStream)
        .pipe(concat('style.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('public/css'));
});

gulp.task('js', function() {
  return gulp.src(['semantic/compiled/semantic.js', 'toastr/toastr.js', 'js/**/*.js'])
    .pipe(sourcemaps.init())
    .pipe(babel())
    .pipe(concat('app.js'))
//    .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('public/js'));
});

gulp.task('locale', function() {
    return gulp.src('locale/**/*.js')
        .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('public/js'));
});

// configure the jshint task
gulp.task('jshint', function() {
  return gulp.src('js/**/*.js')
    .pipe(jshint({esversion: 6}))
    .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('semantic', semantic);

// configure which files to watch and what tasks to use on file changes
//gulp.task('watch', function() {
//  gulp.watch('js/**/*.js', ['jshint']);
//  gulp.watch('js/**/*.js', ['js']);
//  gulp.watch('sass/**/*.scss', ['css']);
//});