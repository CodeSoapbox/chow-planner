(function(){
    "use strict";

    const SETTING_SHOW_HINTS = 'show-hints';

    const EVENT_TOGGLE_HINTS = "toggle.hints";

    function getSettingInput(name) {
        return $('input[name="settings-' + name + '"]');
    }

    $(document)
        .ready(function() {
            getSettingInput(SETTING_SHOW_HINTS).on('click', (event) => {
                const $target = $(event.target);
                const $container = $target.closest('.segment');
                $container.dimmer('show');

                $.ajax({
                    type: "PUT",
                    url: "settings/hints/toggle",
                    contentType:"application/json; charset=utf-8"
                }).then(function(data, textStatus, xhr) {
                    if(xhr.status == 200) {
                        $target.prop('checked', false);
                        if(data) {
                            toastr["success"](translate['get']('toast.hints.enabled'));
                        } else {
                            toastr["success"](translate['get']('toast.hints.disabled'));
                        }
                    } else {
                        toastr["error"](translate['get']('toast.hints.error'));
                    }

                    $(document).trigger(EVENT_TOGGLE_HINTS, data);
                    $container.dimmer('hide');
                });
            });
        })
    ;
})();