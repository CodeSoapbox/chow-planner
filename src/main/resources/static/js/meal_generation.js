(function(){
    "use strict";

    $(document)
      .ready(function() {
            $.fn.mealDynamicList = function(selectedMeals) {
                let template = this.find('[data-list-template="true"]').html();
                this.find('[data-list-template="true"]').remove();

                let mealsDropdown = this.find('select');
                let container = this.find('[data-list-container="true"]');

                mealsDropdown.dropdown();

                const getNumberValue = (number) => {
                    return parseInt(number);
                };

                const addNewHtmlMeal = (mealId, name, quantity, unit) => {
                    let $mealTemplate = $(template);
                    $mealTemplate.attr('data-id', mealId);
                    $mealTemplate.find('[data-value="name"]').html(name);
                    $mealTemplate.find('[data-value="label"]').html(quantity);

                    $mealTemplate.find('.action-delete').on('click', () => {
                        event.preventDefault();
                        container.find('[data-id="' + mealId + '"]').remove();

                        //Remove form data meal
                        const $mealDiv = $("#meals input[name$='id'][value=" + mealId + "]").parent();
                        $mealDiv.remove();

                        //Reorder all form data meals
                        let index = 0;
                        const $mealDivs = $('#meals').children();
                        $mealDivs.each(function() {
                            $(this).find("input[name$='id']").attr('name', "chosenMeals[" + index + "].id");
                            $(this).find("input[name$='quantity']").attr('name', "chosenMeals[" + index + "].quantity");
                            $(this).find("input[name$='name']").attr('name', "chosenMeals[" + index + "].name");
                            index++;
                        });

                        enableOrDisableSubmitButton();
                    });

                    $mealTemplate.find('.action-edit').on('click', () => {
                        const editQuantityModal = $('.edit-meal-quantity.modal');
                        const quantityInput = editQuantityModal.find('#newMealQuantity');

                        const $mealDiv = $("#meals input[name$='id'][value=" + mealId + "]").parent();

                        quantityInput.val(
                            getNumberValue($mealDiv.find("input[name$='quantity']").attr('value'))
                        );

                        editQuantityModal.find('.ok.button').off('click');
                        editQuantityModal.find('.ok.button').on('click', () => {
                            const $mealDiv = $("#meals input[name$='id'][value=" + mealId + "]").parent();
                            $mealDiv.find("input[name$='quantity']").attr('value', quantityInput.val());
                            updateHtmlMeal(mealId, getNumberValue($mealDiv.find("input[name$='quantity']").attr('value')));
                            updateFormDataMeal(
                                mealId,
                                getNumberValue($mealDiv.find("input[name$='quantity']").attr('value'))
                            );
                        });

                        editQuantityModal.modal('show');
                    });

                    container.append($mealTemplate);
                };

                const addNewFormDataMeal = (mealId, name, quantity) => {
                    const $mealDiv = $('<div>');
                    const divIndex = $("#meals").children().length;
                    const prefix = "chosenMeals[" + divIndex + "].";

                    $mealDiv.append($('<input>', {
                        type: 'hidden',
                        name: prefix + 'name',
                        value: name,
                    }));

                    $mealDiv.append($('<input>', {
                        type: 'hidden',
                        name: prefix + 'id',
                        value: mealId,
                    }));

                    $mealDiv.append($('<input>', {
                        type: 'hidden',
                        name: prefix + 'quantity',
                        value: quantity,
                    }));

                    $("#meals").append($mealDiv);
                };

                const updateHtmlMeal = (mealId, quantity, unit) => {
                    container.find('[data-id="' + mealId + '"] [data-value="label"]').html(quantity);
                };

                const updateFormDataMeal = (mealId, quantity, unit) => {
                    const $mealDiv = $("#meals input[name$='id'][value=" + mealId + "]").parent();
                    $mealDiv.find("input[name$='quantity']").attr('value', quantity);
                };

                const redrawHtmlList = (selectedMeals) => {
                    $.each(selectedMeals, (id, meal) => {
                        addNewHtmlMeal(id, meal.name, getNumberValue(meal.quantity));
                    });
                };

                redrawHtmlList(selectedMeals);

                const enableOrDisableSubmitButton = () => {
                    const $btn = $('#generate-grocery-list-btn');
                    const $mealDivs = $('#meals').children();
                    if($mealDivs.length < 1) {
                        $btn.addClass('disabled');
                    } else {
                        $btn.removeClass('disabled');
                    }
                };
                enableOrDisableSubmitButton();

                this.find('button').on('click', () => {
                    event.preventDefault();

                    const mealId = mealsDropdown.val();
                    const mealOption = $('option[value="' + mealId + '"]');
                    const name = mealOption.data('name');
                    let quantity = this.find('#mealQuantity').val();

                    if($("#meals input[name$='id'][value=" + mealId + "]").length < 1) {
                        addNewHtmlMeal(mealId, name, quantity);
                        addNewFormDataMeal(mealId, name, quantity);
                        enableOrDisableSubmitButton();
                    } else {
                        const $mealDiv = $("#meals input[name$='id'][value=" + mealId + "]").parent();
                        quantity = parseFloat(quantity) + parseFloat($mealDiv.find("input[name$='quantity']").attr('value'));
                        updateHtmlMeal(mealId, getNumberValue(quantity));
                        updateFormDataMeal(mealId, getNumberValue(quantity));
                        enableOrDisableSubmitButton();
                    }
                });
            };

            // Init meal list
            if($('#mealList').length) {
                const $meals = $("#meals");
                if(typeof $meals !== 'undefined') {
                    let selectedMeals = {};
                    $meals.children().each(function()
                    {
                        selectedMeals[$(this).find("input[name$='id']").attr('value')] = {
                            'name': $(this).find("input[name$='name']").attr('value'),
                            'quantity': $(this).find("input[name$='quantity']").attr('value')
                        };
                    });
                    $('#mealList').mealDynamicList(selectedMeals);
                } else {
                    console.warn("Meal list component found in DOM but no meals");
                }
            }
      })
    ;
})();