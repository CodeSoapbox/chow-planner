(function(){
    "use strict";

    $(document)
        .ready(function() {
//            toastr.options.closeButton = true;
//            toastr.options.progressBar = true;
            toastr.options.timeOut = 5000; // How long the toast will display without user interaction
            toastr.options.extendedTimeOut = 2000; // How long the toast will display after a user hovers over it
        })
    ;
})();