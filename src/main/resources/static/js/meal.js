(function(){
    "use strict";

    $(document)
      .ready(function() {
            $.fn.ingredientDynamicList = function(selectedIngredients) {
                let template = this.find('[data-list-template="true"]').html();
                this.find('[data-list-template="true"]').remove();

                let ingredientDropdown = this.find('select');
                let container = this.find('[data-list-container="true"]');

                ingredientDropdown.dropdown();

                const getNumberValue = (number) => {
                    return parseFloat(number).toString().replace(/[.,]00$/, "");
                };

                const addNewHtmlIngredient = (ingredientId, name, quantity, unit) => {
                    let ingredientTemplate = $(template);
                    ingredientTemplate.attr('data-id', ingredientId);
                    ingredientTemplate.find('[data-value="name"]').html(name);
                    ingredientTemplate.find('[data-value="label"]').html(quantity + ' ' + unit);

                    ingredientTemplate.find('.action-delete').on('click', () => {
                        event.preventDefault();
                        container.find('[data-id="' + ingredientId + '"]').remove();

                        //Remove form data item
                        const $ingredientDiv = $("#ingredients input[name$='id'][value=" + ingredientId + "]").parent();
                        $ingredientDiv.remove();

                        //Reorder all form data items
                        let index = 0;
                        const $ingredientDivs = $('#ingredients').children();
                        $ingredientDivs.each(function() {
                            $(this).find("input[name$='id']").attr('name', "ingredients[" + index + "].item.id");
                            $(this).find("input[name$='quantity']").attr('name', "ingredients[" + index + "].quantity");
                            $(this).find("input[name$='name']").attr('name', "ingredients[" + index + "].item.name");
                            $(this).find("input[name$='unit']").attr('name', "ingredients[" + index + "].item.unit");
                            index++;
                        });
                    });

                    ingredientTemplate.find('.action-edit').on('click', () => {
                        let editQuantityModal = $('.edit-ingredient-quantity.modal');
                        let quantityInput = editQuantityModal.find('#newIngredientQuantity');

                        const ingredientDiv = $("#ingredients input[name$='id'][value=" + ingredientId + "]").parent();

                        quantityInput.val(
                            getNumberValue(ingredientDiv.find("input[name$='quantity']").attr('value'))
                        );

                        editQuantityModal.find('.ok.button').off('click');
                        editQuantityModal.find('.ok.button').on('click', () => {
                            const $ingredientDiv = $("#ingredients input[name$='id'][value=" + ingredientId + "]").parent();
                            $ingredientDiv.find("input[name$='quantity']").attr('value', quantityInput.val());
                            updateHtmlItem(ingredientId, getNumberValue($ingredientDiv.find("input[name$='quantity']").attr('value')), $ingredientDiv.find("input[name$='unit']").attr('value'));
                            updateFormDataItem(
                                ingredientId,
                                getNumberValue($ingredientDiv.find("input[name$='quantity']").attr('value')),
                                $ingredientDiv.find("input[name$='unit']").attr('value')
                            );
                        });

                        editQuantityModal.modal('show');
                    });

                    container.append(ingredientTemplate);
                };

                const addNewFormDataIngredient = (ingredientId, name, quantity, unit) => {
                    const $ingredientDiv = $('<div>');
                    const divIndex = $("#ingredients").children().length;
                    const prefix = "ingredients[" + divIndex + "].";

                    $ingredientDiv.append($('<input>', {
                        type: 'hidden',
                        name: prefix + 'item.name',
                        value: name,
                    }));

                    $ingredientDiv.append($('<input>', {
                        type: 'hidden',
                        name: prefix + 'item.id',
                        value: ingredientId,
                    }));

                    $ingredientDiv.append($('<input>', {
                        type: 'hidden',
                        name: prefix + 'quantity',
                        value: quantity,
                    }));

                    $ingredientDiv.append($('<input>', {
                        type: 'hidden',
                        name: prefix + 'item.unit',
                        value: unit,
                    }));

                    $("#ingredients").append($ingredientDiv);
                };

                const updateHtmlItem = (ingredientId, quantity, unit) => {
                    container.find('[data-id="' + ingredientId + '"] [data-value="label"]').html(quantity + ' ' + unit);
                };

                const updateFormDataItem = (ingredientId, quantity, unit) => {
                    const $ingredientDiv = $("#ingredients input[name$='id'][value=" + ingredientId + "]").parent();
                    $ingredientDiv.find("input[name$='quantity']").attr('value', quantity);
                    $ingredientDiv.find("input[name$='unit']").attr('value', unit);
                };

                const redrawHtmlList = (selectedIngredients) => {
                    $.each(selectedIngredients, (id, item) => {
                        addNewHtmlIngredient(id, item.name, getNumberValue(item.quantity), item.unit);
                    });
                };

                redrawHtmlList(selectedIngredients);

                this.find("button[name='ingredientAddBtn']").on('click', () => {
                    event.preventDefault();

                    const ingredientId = ingredientDropdown.val();
                    const itemOption = $('option[value="' + ingredientId + '"]');
                    const name = itemOption.data('name');
                    let quantity = this.find('#ingredientQuantity').val();
                    let unit = itemOption.data('unit');
                    if(typeof unit == 'undefined') {
                        unit = "";
                    }

                    if($("#ingredients input[name$='id'][value=" + ingredientId + "]").length < 1) {
                        addNewHtmlIngredient(ingredientId, name, quantity, unit);
                        addNewFormDataIngredient(ingredientId, name, quantity, unit);
                    } else {
                        const ingredientDiv = $("#ingredients input[name$='id'][value=" + ingredientId + "]").parent();
                        quantity = parseFloat(quantity) + parseFloat(ingredientDiv.find("input[name$='quantity']").attr('value'));
                        updateHtmlItem(ingredientId, getNumberValue(quantity), unit);
                        updateFormDataItem(ingredientId, getNumberValue(quantity), unit);
                    }
                });
            };

            // Init ingredient list
            if($('#ingredientList').length) {
                const $ingredients = $("#ingredients");
                if(typeof $ingredients !== 'undefined') {
                    let selectedIngredients = {};
                    $ingredients.children().each(function()
                    {
                        let unit = $(this).find("input[name$='unit']").attr('value');
                        if(typeof(unit) == 'undefined') {
                            unit = '';
                        }

                        selectedIngredients[$(this).find("input[name$='id']").attr('value')] = {
                            'name': $(this).find("input[name$='name']").attr('value'),
                            'quantity': $(this).find("input[name$='quantity']").attr('value'),
                            'unit': unit
                        };
                    });
                    $('#ingredientList').ingredientDynamicList(selectedIngredients);
                } else {
                    console.warn("Ingredient list component found in DOM but no ingredients");
                }
            }
      })
    ;
})();