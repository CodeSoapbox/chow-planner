
(function (define) {
    define(['jquery'], function ($) {
        let translations = {};

        const add = (template, translation) => {
            if(typeof template === 'object') {
                translations = Object.assign(translations, template);
                return;
            }
            translations[template] = translation;
        };

        const get = (template) => {
            if(typeof translations[template] == 'undefined') {
                console.warn("No translation found for: " + template);
                return template;
            }
            return translations[template];
        };

        const translate = {
            add: add,
            get: get
        };

        return translate;
    });
}(typeof define === 'function' && define.amd ? define : function (deps, factory) {
     if (typeof module !== 'undefined' && module.exports) { //Node
         module.exports = factory(require('jquery'));
     } else {
         window.translate = factory(window.jQuery);
     }
 }));