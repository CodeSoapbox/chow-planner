(function(){
    "use strict";

    $(document)
      .ready(function() {
            $.fn.addItemModal = function() {
                const $ingredientAddItemBtn = $("button[name='ingredientAddItemBtn']");

                $ingredientAddItemBtn.on('click', () => {
                    event.preventDefault();
                    let addItemModal = $('.add-item.modal');

                    addItemModal.find('.ok.button').off('click');
                    addItemModal.find('.ok.button').on('click', () => {
                        alert("To be developed!");
                    });
                    addItemModal.modal('show');
                });
            };

            $('#ingredientList').addItemModal();
      }
    );
})();