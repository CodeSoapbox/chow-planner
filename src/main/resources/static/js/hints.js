(function(){
    "use strict";

    const DISABLE_HINTS_BUTTON = '.disable.all.hints.button';
    const HINT = '.hint.segment';

    const SETTING_SHOW_HINTS = 'show-hints';

    const EVENT_TOGGLE_HINTS = "toggle.hints";

    function getSettingInput(name) {
        return $('input[name="settings-' + name + '"]');
    }

    $(document)
        .ready(function() {

            $(document).on( EVENT_TOGGLE_HINTS, ( event, enabled ) => {
                getSettingInput(SETTING_SHOW_HINTS).prop('checked', enabled);

                if(!enabled) {
                    $(HINT).hide();
                } else {
                    $(HINT).show();
                }
            });

            $(DISABLE_HINTS_BUTTON).on('click', (event) => {
                event.preventDefault();

                const $target = $(event.target);
                const $container = $target.closest('.hint.segment');
                $container.dimmer('show');

                $.ajax({
                    type: "PUT",
                    url: "settings/hints/toggle",
                    contentType:"application/json; charset=utf-8",
                    dataType:"json",
                    data: JSON.stringify({
                        enabled: false
                    })
                }).then(function(data, textStatus, xhr) {
                    if(xhr.status == 200) {
                        if(data == false) {
                            toastr["success"](translate['get']('toast.hints.disabled'));
                        } else {
                            toastr["error"](translate['get']('toast.hints.error'));
                        }
                    } else {
                        toastr["error"](translate['get']('toast.hints.error'));
                    }

                    $(document).trigger(EVENT_TOGGLE_HINTS, data);
                    $container.dimmer('hide');
                });
            });
        })
    ;
})();