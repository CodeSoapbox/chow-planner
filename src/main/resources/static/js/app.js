(function(){
    "use strict";

    $(document)
      .ready(function() {
        // create sidebar and attach to menu open
        $('.ui.sidebar.menu')
          .sidebar('attach events', '.toc.item')
        ;

        $(".ui.dropdown").dropdown();

        $('#mobileMenu').show();
        $('#mobileMenu').sticky();
        $('#mobileMenu').attr('style', '');

        $('.message .close')
        .on('click', function() {
          $(this)
            .closest('.message')
            .transition('fade')
          ;
        })
        ;

        $("a[data-modal='confirm']").on('click', function(event) {
          event.preventDefault();

          let href = $(this).attr('href');

          $('.ui.confirm.modal .ok.button').on('click', function() {
            location.href = href;
          });

          $('.ui.confirm.modal').modal('show');
        });
      })
    ;
})();